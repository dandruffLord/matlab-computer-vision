function [boundaryIm] = boundaryPixels(labelIm)
%boundaryPixels Detects cluster boundary pixels
boundaryIm = zeros(size(labelIm,1), size(labelIm,2));
r = 2;
while r <= size(labelIm,1) - 1
    c = 2;
    while c <= size(labelIm,2) - 1
        if((labelIm(r,c) ~= labelIm(r+1,c)) || (labelIm(r,c) ~= labelIm(r-1,c)) || (labelIm(r,c) ~= labelIm(r+1,c+1)) || (labelIm(r,c) ~= labelIm(r,c-1)))
            boundaryIm(r,c) = 1;
        end
        c = c + 1;
    end
    r = r + 1;
end
end

