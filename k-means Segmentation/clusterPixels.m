function [labelIm] = clusterPixels(Im, k)
%clusterPixels Applies k-means clustering to associate pixels with clusters
im = im2double(rgb2gray(Im));

filters = [[1,1,1;0,0,0;-1,-1,-1];[1,0,-1;1,0,-1;1,0,-1];[1,1,0;1,0,-1;0,-1,-1];[0,1,1;-1,0,1;-1,-1,0]];
dimensionCount = size(filters,1)/3;
actualFeatureSpace = zeros(size(im,1),size(im,2),dimensionCount);
i = 1;
while i <= dimensionCount
    actualFeatureSpace(:,:,i) = conv2(im, filters(((i-1)*3)+1:((i-1)*3)+3,:),'same');
    i = i + 1;
end

windowSize = 54;
windowWidth = round(windowSize*(size(Im,1)/size(Im,2)));
windowLength = round(windowSize*(size(Im,2)/size(Im,1)));
reduce = @(block_struct) mean(mean(block_struct.data));
featureSpaceSize = size(blockproc(actualFeatureSpace(:,:,1), [windowWidth, windowLength], reduce));
featureSpace = zeros(featureSpaceSize(1,1),featureSpaceSize(1,2),dimensionCount+3);
i = 1;
while i <= dimensionCount
    featureSpace(:,:,i) = blockproc(actualFeatureSpace(:,:,i), [windowWidth, windowLength], reduce);
    i = i + 1;
end

im = im2double(Im);
dimensionCount = dimensionCount + 3;
featureSpace(:,:,5) = blockproc(im(:,:,1), [windowWidth, windowLength], reduce);
featureSpace(:,:,6) = blockproc(im(:,:,2), [windowWidth, windowLength], reduce);
featureSpace(:,:,7) = blockproc(im(:,:,3), [windowWidth, windowLength], reduce);

centers = zeros(k,dimensionCount);
centers(1,:) = featureSpace(round(rand*size(featureSpace,1)), round(rand*size(featureSpace,2)), :);
i = 1;
randomCenter = 1;
while i < k
    distances = zeros(size(featureSpace,1),size(featureSpace,2));
    r = 1;
    while r <= size(featureSpace,1)
        c = 1;
        while c <= size(featureSpace,2)
            i2 = 1;
            while i2 <= i
                distances(r,c) = distances(r,c) + sqrt(pdist([centers(i2,:);reshape(featureSpace(r,c,:),[1,dimensionCount])]));
                i2 = i2 + 1;
            end
            c = c + 1;
        end
        r = r + 1;
    end
    [~,maxIndex] = max(distances(:));
    [maxRow,maxCol] = ind2sub(size(distances),maxIndex);
    if randomCenter == 1
        centers(1,:) = featureSpace(maxRow,maxCol,:);
        randomCenter = 0;
    else
        centers(i+1,:) = featureSpace(maxRow,maxCol,:);
        i = i + 1;
    end
end

oldCenters = zeros(k,dimensionCount);
clusters = zeros(size(featureSpace,1),size(featureSpace,2));
duplicate = 0;

while duplicate == 0
    oldCenters = cat(3,oldCenters,centers);
    r = 1;
    while r <= size(featureSpace,1)
        c = 1;
        while c <= size(featureSpace,2)
            i = 1;
            distance = realmax;
            while i <= size(centers,1)
                if norm(centers(i,:) - reshape(featureSpace(r,c,:),[1,dimensionCount])) < distance
                    distance = sqrt(pdist([centers(i,:);reshape(featureSpace(r,c,:),[1,dimensionCount])]));
                    clusters(r,c) = i;
                end
                i = i + 1;
            end
            c = c + 1;
        end
        r = r + 1;
    end
    
    i = 1;
    while i <= size(centers,1)
        [pointRows, pointCols] = find(clusters==i);
        meanVector = ones(size(pointRows,1),1);
        i2 = 1;
        while i2 <= size(pointRows,1)
            i3 = 1;
            while i3 <= dimensionCount
                meanVector(i2,1) = meanVector(i2,1)*(featureSpace(pointRows(i2), pointCols(i2),i3)/dimensionCount);
                i3 = i3 + 1;
            end
            i2 = i2 + 1;
        end
        [~,centerIndex] = min(abs(meanVector - mean(meanVector)));
        centers(i,:) = featureSpace(pointRows(centerIndex),pointCols(centerIndex),:);
        i = i + 1;
    end
    
    i = 1;
    while i <= size(oldCenters,3)
        if isequal(oldCenters(:,:,i),centers)
            size(oldCenters,3)
            duplicate = 1;
            break
        end
        i = i + 1;
    end
end

expand = @(block_struct) ones(windowWidth, windowLength)*block_struct.data(1,1);
labelIm = blockproc(clusters, [1, 1], expand);
end