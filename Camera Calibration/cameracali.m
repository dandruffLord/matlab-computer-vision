function [K,R,T] = cameracali(Coord2d, Coord3d)
%cameracali Determines the intrinsic and extrinsic parameters of a camera 
%by using feature points from an image of a rig

%Calculate M matrix
M = zeros(size(Coord2d,2)*2,12);
i = 1;
while i <= size(Coord2d,2)
    M((i*2)-1,:) = cat(2,transpose(Coord3d(:,i)),1,[0,0,0,0],transpose(-Coord2d(1,i)*Coord3d(:,i)),-Coord2d(1,i));
    M(i*2,:) = cat(2,[0,0,0,0],transpose(Coord3d(:,i)),1,transpose(-Coord2d(2,i)*Coord3d(:,i)),-Coord2d(2,i));
    i = i + 1;
end

%Estimate pi matrix
[eigenVector, ~] = eigs(transpose(M)*M,1,'smallestabs');
u1 = eigenVector(1:3);
u2 = eigenVector(5:7);
u3 = eigenVector(9:11);
if det(transpose([u1,u2,u3])) < 0
    u1 = -u1;
    u2 = -u2;
    u3 = -u3;
    eigenVector = -eigenVector;
end

%Exrtact K, R, and T matrices
k33 = norm(u3);
r3 = u3/k33;
k23 = transpose(u2)*r3;
k22 = norm(u2 - k23*r3);
r2 = (u2 - k23*r3)/k22;
k13 = transpose(u1)*r3;
k12 = transpose(u1)*r2;
k11 = norm(u1 - k12*r2 - k13*r3);
r1 = (u1 - k12*r2 - k13*r3)/k11;

K = [k11,k12,k13;0,k22,k23;0,0,k33]/k33;
R = transpose([r1,r2,r3]);
T = inv(K)*[eigenVector(4);eigenVector(8);eigenVector(12)];
end