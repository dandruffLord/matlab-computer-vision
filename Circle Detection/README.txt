detectCirclesHT.m: Takes an input image and a fixed (known) radius, and returns the centers of any detected circles of about that size using Hough Transformation

detectCirclesRANSAC.m: Takes an input image and a fixed (known) radius, and returns the centers of any detected circles of about that size using RANSAC

Hyperparameters have to be tuned in order to produce a sufficient result