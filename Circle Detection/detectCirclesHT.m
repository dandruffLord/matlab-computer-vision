function [centers] = detectCirclesHT(im, radius)
%detectCirclesHT Takes an input image and a fixed (known) radius, 
%and returns the centers of any detected circles of about that size
%using Hough Transformation
BW = edge(rgb2gray(im));
[edgeRows, edgeCols] = find(BW);
H = zeros(size(BW));
directionChange = 1.0/radius;
voteValue = 255/size(edgeRows,1);

i = 1;
while i <= size(edgeRows,1)
    x = edgeRows(i);
    y = edgeCols(i);
    direction = 0.0;
    while direction < 2*pi
        a = x + round(radius*cos(direction));
        b = y - round(radius*sin(direction));
        if all([a, b] <= size(H)) && all([a, b] >= 1)
            H(a,b) = H(a,b) + voteValue;
        end
        direction = direction + directionChange;
    end
    i = i + 1;
end

binDim = 10;
binWidth = round((size(H,1)/size(H,2))*binDim);
binLength = round((size(H,2)/size(H,1))*binDim);
bin = @(block_struct) sum(block_struct.data(block_struct.data>voteValue));
binArray = blockproc(H, [binWidth, binLength], bin);
imshow(transpose(binArray*(max(max(binArray))/255)));
level = max(max(binArray))*graythresh(binArray);
[centerRows, centerCols] = find(imbinarize(binArray,level));
centerCols = (centerCols*binLength) - binLength/2;
centerRows = (centerRows*binWidth) - binWidth/2;
centers = [centerCols, centerRows];
end

