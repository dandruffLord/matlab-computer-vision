function [centers] = detectCirclesRANSAC(im, radius)
%detectCirclesRANSAC Takes an input image and a fixed (known) radius, 
%and returns the centers of any detected circles of about that size
%using RANSAC
BW = edge(rgb2gray(im));
[edgeRows, edgeCols] = find(BW);
iterations = 10000;
inlierCounts = zeros(1, iterations);
centers = zeros(iterations,2);

n = 1;
inlierThreshold = 2;
while n <= iterations
    sampleIndices = rand(1,4);
    while isequal(find(sampleIndices==sampleIndices(1,1)),[1,1,1,1])
        sampleIndices = rand(1,4);
    end
    sampleIndices = ceil(sampleIndices*size(edgeRows,1));
    x0 = edgeCols(sampleIndices(1,1));
    y0 = edgeRows(sampleIndices(1,2));
    x1 = edgeCols(sampleIndices(1,3));
    y1 = edgeRows(sampleIndices(1,4));
    v = [x1,y1] - [x0,y0];
    u = v/norm(v);
    center = [x0,y0] + radius*u;
    
    i = 1;
    distances = zeros(1,size(edgeRows,1));
    while i <= size(edgeRows,1)
        distance = abs(radius - norm(center - [edgeCols(i,1),edgeRows(i,1)]));
        distances(1,i) = distance;
        i = i + 1;
    end
    
    inlierCount = size(distances(distances<inlierThreshold),2);
    inlierCounts(1,n) = inlierCount;
    centers(n,:) = center;
    n = n + 1;
end

[~,I] = maxk(inlierCounts,15);
a = centers(:,1);
a = a(I);
b = centers(:,2);
b = b(I);
centers = [a,b];
end

