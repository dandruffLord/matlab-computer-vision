function [output] = removeVertical(im, numPixels)
%removeVertical Takes an input image im, and a parameter specifying 
%how many seams to carve from the width
[output,~] = removeSeams(im, numPixels, 1, 'sobel');
end

