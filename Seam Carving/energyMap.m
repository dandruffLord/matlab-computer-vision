function [E] = energyMap(I, filter)
%energyMap Computes the energy function at each pixel using 
%the magnitude of the x and y gradients
[Gmag,~] = imgradient(imgaussfilt(im2double(rgb2gray(I))), filter);
E = Gmag;
end