costMap.m: Computes the gradient cost map of an energy map

displaySeams.m: Displays the selected seams on top of an image

energyMap.m: Computes the energy function at each pixel using the magnitude of the x and y gradients (equation 1 in the paper)

horizontalSeam.m: Computes the optimal horizontal seam given an image

verticalSeam.m: Computes the optimal vertical seam given an image

removeSeams.m: Reduce the image size by a specified amount in one dimension (width or height decrease)

removeHorizontal.m: Takes an input image im, and a parameter specifying how many seams to carve from the height

removeVertical.m: Takes an input image im, and a parameter specifying how many seams to carve from the width

returnSeams.m: Takes an input image im, and a parameter specifying how many seams to carve from the given direction

seam.m: Computes the optimal seam given a gradient cost map and direction


In removeSeams.m, I’ve added a seamDif variable which affects how many seams are carved before the energy map is recomputed, specifically recomputing before the current seam based on its accumulated proximity to previous seams after the latest computation of the energy map.