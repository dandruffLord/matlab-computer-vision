function [S] = seam(M,X)
%seam Computes the optimal seam given a gradient cost map and direction
if X == 1
    Y = 2;
else
    Y = 1;
end
i = size(M,1);
S = zeros(i,2);
[~,root] = min(M(i,:));
S(i,X) = i;
S(i,Y) = root;
while i ~= 1
    i = i-1;
    if eq(root,1)
        start = 1;
        root = root + 1;
    else
        start = root-1;
    end
    if eq(root,size(M,2))
        finish = size(M,2);
    else
        finish = root+1;
    end
    [~,index] = min(M(i,start:finish));
    root = root - 2 + index;
    S(i,X) = i;
    S(i,Y) = root;
end