function [output, seams] = returnSeams(im, numPixels, direction, filter)
%returnSeams Takes an input image im, and a parameter specifying how
%many seams to carve from the given direction
if strcmp(direction,'vertical')
    [output, seams] = removeSeams(im, numPixels, 1, filter);
elseif strcmp(direction,'horizontal')
    [output,seams] = removeSeams(permute(im,[2 1 3]), numPixels, 2, filter);
    output = permute(output, [2 1 3]);
end