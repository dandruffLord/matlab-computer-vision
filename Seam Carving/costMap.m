function [C] = costMap(G)
%costMap Computes the gradient cost map of an energy map
r = 1;
while r < size(G, 1)
    G(r+1,1) = G(r+1,1) + min([G(r,1),G(r,2)]);
    c = 2;
    while c < size(G, 2)
        G(r+1,c) = G(r+1,c) + min([G(r,c-1),G(r,c),G(r,c+1)]);
        c = c + 1;
    end
    G(r+1,c) = G(r+1,c) + min([G(r,c-1),G(r,c)]);
    r = r + 1;
end
C = G;
end

