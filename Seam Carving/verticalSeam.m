function [S] = verticalSeam(I)
%verticalSeam Computes the optimal vertical seam given an image
M = costMap(energyMap(I, 'sobel'));
S = seam(M,1);
end