function [output] = removeHorizontal(im, numPixels)
%removeHorizontal Takes an input image im, and a parameter specifying how
%many seams to carve from the height
[output,~] = removeSeams(permute(im,[2 1 3]), numPixels, 2, 'sobel');
output = permute(output, [2 1 3]);
end