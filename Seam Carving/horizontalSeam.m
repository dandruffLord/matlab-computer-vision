function [S] = horizontalSeam(I)
%verticalSeam Computes the optimal horizontal seam given an image
M = costMap(transpose(energyMap(I, 'sobel')));
S = seam(M,2);
end