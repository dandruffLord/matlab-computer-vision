function [output, seams] = removeSeams(im, numPixels, X, filter)
%removeVertical Takes an input image im, and a parameter specifying how
%many seams to carve from the given direction
if X == 1
    Y = 2;
else
    Y = 1;
end

oldS = (size(im, 2) + 1) * ones(size(im, 1),numPixels*2);
seamDif = 0;
M = costMap(energyMap(im, filter));
while numPixels ~= 0
    if seamDif > 0
        seamDif = 0;
        M = costMap(energyMap(im, filter));
    end
    S = seam(M,X);
    i = 1;
    newMap = zeros(size(M,1),size(M,2)-1);
    newIm = zeros(size(im,1),size(im,2)-1,size(im,3),'uint8'); 
    while i <= size(M,1)
        oldPoint = S(i,Y);
        S(i,Y) = S(i,Y) + (S(i,Y) >= oldS(i,numPixels*2));
        seamDif = seamDif + abs(S(i,Y) - oldS(i,numPixels*2))/(size(M,1)*size(M,2));
        newMap(i,:) = cat(2,M(i,1:oldPoint-1),M(i,oldPoint+1:end));
        newIm(i,:,:) = cat(2,im(i,1:oldPoint-1,:),im(i,oldPoint+1:end,:));
        i = i + 1;
    end
    M = newMap;
    im = newIm;
    oldS(:,(numPixels*2)-1:numPixels*2) = S;
    numPixels = numPixels - 1;
end
seams = oldS;
output = im;
end