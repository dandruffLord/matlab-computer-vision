function [] = displaySeams(I, S)
%displaySeams Displays the selected seams on top of an image
imshow(I);
hold on;
i = 1;
while i < size(S,2)
    plot(S(:,i + 1),S(:,i),'r');
    i = i + 2;
end
hold off;
end

